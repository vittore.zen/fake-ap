#!/bin/bash

apt-get update
apt-get -y upgrade
apt-get -y dist-upgrade
uname -a
apt-get -y install git gettext make intltool build-essential automake \
autoconf uuid uuid-dev dos2unix curl sudo unzip lsb-release python-scapy \
tcpdump python-netifaces python-pip git ntp dnsmasq  hostapd libssl-dev \
wireless-tools iw build-essential autoconf automake libtool \
pkg-config libnl-3-dev libnl-genl-3-dev libssl-dev ethtool shtool \
rfkill zlib1g-dev libpcap-dev libsqlite3-dev libpcre3-dev \
libhwloc-dev libcmocka-dev
apt-get install -y ntpdate
ntpdate time.ien.it

apt-get -y install libssl1.0
apt-get -y remove python-cryptography
pip install mitmproxy

cd /root
git clone https://github.com/xtr4nge/FruityWifi.git --depth=1
cd /root/FruityWifi
chmod a+x install-FruityWiFi.sh
./install-FruityWiFi.sh
