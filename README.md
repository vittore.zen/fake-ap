1. Installare su una micro SSD raspbian lite
2. nella partizione boot creare un file vuoto dal nome ssh
3. collegare la raspberry alla rete con il cavo
4. accendere la raspberry
5. Nel router verificare che indirizzo ip è stato assegnato alla raspberry
6. collegarsi alla raspberry con il comando
```
 ssh pi@indirizzo_ip_assegnato
``` 
7. la password è raspberry
8. entrare in modalità root con il comando sudo su
9. scaricare ed eseguire lo script di installazione con i comandi:
```
wget "https://gitlab.com/vittore.zen/fake-ap/-/raw/main/script.sh?inline=false" -O script.sh
sh script.sh
```
Ad installazione conclusa fare login al sistema via browser:
http://indirizzo_ip_assegnato:8000
- Username: admin
- Password: admin
Andare nella sezione modules
(questa azione non installa i moduli ma li prepara per l'installazione)
-> List available modules [tar.gz]
Premere il link install in corrispondenza dei moduli:
- ap
- urlsnarf
- sslstrip2
- fruityproxy

Andare nella sezione modules, in corrispondenza di ogni modulo scelto precedentemente premere il link View
Se nel modulo è presente un link "install" premere il link install
